"""
Django settings for portal project.

Generated by 'django-admin startproject' using Django 3.0.7.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.0/ref/settings/
"""
from .base import *


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'bs8yzkvn+v2*v6tcl#qm95ofpfd#d2b$xwf1%i0nz_dc37y7(@'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

DATABASES = {
   'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'cogdemo',
        'USER': 'postgres',
        'PASSWORD': 'cognizance1234',
        'HOST': 'cognizance.cmnrhwsxx8p1.eu-west-1.rds.amazonaws.com',
        'PORT': '5432',
    }
}

EXECUTABLE_FILE_NAME="spoon.sh"
TEMP_XAF_FOLDER = "/opt/temp/"
