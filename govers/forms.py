from django import forms
from .models import EventLog, CaseWareFile
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import CustomUser

from django import forms


class XAFForm(forms.ModelForm):
    class Meta:
        model = EventLog
        fields = ['xaffile']


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = CustomUser
        fields = ('email',)


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = ('email',)


class LoginForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = ['email']


class CasewareForm(forms.Form):
    class Meta:
        model = CaseWareFile
        fields = ['casewarefile']
