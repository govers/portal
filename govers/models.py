from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _

from .managers import CustomUserManager
from django.conf import settings
from django.db import models
from django.core.files.storage import FileSystemStorage
import uuid


class CustomStorage(FileSystemStorage):
    def get_valid_name(self, name):
        return name

def formValidName(instance, filename):
    source = instance.source
    companyname = instance.companyname
    fiscalyear= instance.fiscalyear
    uniqueFolderName = str(uuid.uuid4())
    filename = "xafs/" + str(fiscalyear).strip() + "/"+ uniqueFolderName + "/" + str(source).strip() + "_auditbestand_" + str(companyname).strip() + "_" + str(fiscalyear).strip() + ".xaf"
    return filename

class CustomUser(AbstractUser):
    # username = models.CharField(max_length = 50, unique=True)
    email = models.EmailField(_('email address'), null=True, blank=True, unique=True)
    # USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']
    objects = CustomUserManager()

    def __str__(self):
        return self.username


class KtrLog(models.Model):
    objects = None
    id_job = models.IntegerField(primary_key=True)
    channel_id = models.CharField(max_length=250)
    jobname = models.CharField(max_length=250)
    status = models.CharField(max_length=250)
    lines_read = models.BigIntegerField(default=0)
    lines_written = models.BigIntegerField(default=0)
    lines_updated = models.BigIntegerField(default=0)
    lines_input = models.BigIntegerField(default=0)
    lines_output = models.BigIntegerField(default=0)
    lines_rejected = models.BigIntegerField(default=0)
    errors = models.CharField(max_length=250)
    startdate = models.DateTimeField(default="01/01/1900")
    enddate = models.DateTimeField(default="01/01/1900")
    logdate = models.DateTimeField(default="01/01/1900")
    depdate = models.DateTimeField(default="01/01/1900")
    replaydate = models.DateTimeField(default="01/01/1900")
    log_field = models.TextField()


class EventLog(models.Model):
    objects = None
    processedon = models.DateTimeField(auto_now_add=True)
    # processedby = models.ForeignKey(User, on_delete=models.CASCADE)
    filename = models.CharField(max_length=250)
    status = models.CharField(max_length=100)
    source = models.CharField(max_length=50)
    createdon = models.CharField(max_length=50, default="1900-01-01")
    companyname = models.CharField(max_length=250)
    startyear = models.CharField(max_length=50)
    endyear = models.CharField(max_length=50)
    fiscalyear = models.CharField(max_length=20)
    startmonth = models.CharField(max_length=20)
    endmonth = models.CharField(max_length=20)
    filesize = models.BigIntegerField(default=0)
    totalrecords = models.BigIntegerField(default=0)
    xaffile = models.FileField(upload_to=formValidName, storage=CustomStorage())
    uid = models.CharField(max_length=200, unique=True)
    uploadedby = models.ForeignKey(CustomUser, null=True, blank=True, on_delete=models.SET_NULL)
    batchStatus = models.CharField(max_length=150)
    batch_id = models.BigIntegerField(default=0)

    def __str__(self):
        return self.filename


class ProcessingLog(models.Model):
    uid = models.ForeignKey(EventLog, on_delete=models.CASCADE)
    eventtype = models.CharField(max_length=100)
    eventname = models.CharField(max_length=100)
    description = models.CharField(max_length=250)
    status = models.CharField(max_length=100)
    datetime = models.DateTimeField(auto_now_add=True)


class Profile(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')

    def __str__(self):
        return f'{self.user.username} Profile'

class CaseWareFile(models.Model):
    objects = None
    filename = models.CharField(max_length=250)
    casewarefile = models.FileField(upload_to='temp/%Y%m', storage=CustomStorage())
    uploadedon = models.DateTimeField(auto_now_add=True)
    uploadedby = models.ForeignKey(CustomUser, null=True, blank=True, on_delete=models.SET_NULL)
    status = models.CharField(max_length=250,  default="Not Processed")
