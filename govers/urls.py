from django.contrib.auth import views as auth_views
from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='govers-home'),
    path('dashboard/', views.dashboard, name='govers-dashboard'),
    path('yuki/', views.yuki, name='yuki'),
    path('xaf-upload/', views.uploadxaf, name='xaf-upload'),
    path('upload/', views.xafList, name='xaf-list'),
    path('delete/<int:pk>', views.deletexaf, name='xaf-delete'),
    path('delete-caseware/<int:pk>', views.deletecaseware, name='caseware-delete'),
    path('login/', views.customLogin, name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='govers/logout.html'), name='logout'),
    path('process-xaf/', views.processxaf, name='process-xaf'),
    path('add-xaf/<int:pk>', views.addToBatch, name='add'),
    path('remove-xaf/<int:pk>', views.removeFromBatch, name='remove'),
    path('add-caseware/<int:pk>', views.addCasewareTobatch, name='addcaseware'),
    path('remove-caseware/<int:pk>', views.removeCasewareFromBatch, name='removecaseware'),
    path('processing-details/<int:pk>', views.processingdetails, name='processingdetails'),
    path('profile/', views.viewprofile, name='profile'),
    path('permissiondenied/', views.permission_denied, name='permissionDenied'),
    path('rgscodes/', views.missingrgsidPage, name='missing-rgscodes'),
    path('caseware/', views.casewarePage, name='caseware-page'),
    path('rgscodes/generate/', views.getMissingRGSIds, name='generate-missing-rgscodes'),
    path('upload-schema', views.uploadCaswareSchema, name='caseware-schema'),
    path('caseware-check', views.CaswareSchemaCheck, name='caseware-schema-check'),
    path('rgscodes/upload', views.uploadcaseware, name='caseware-upload'),

]
