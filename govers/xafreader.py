import xml.etree.ElementTree as ET
import os

import sys
from hashlib import md5
import hashlib

import re
from datetime import datetime


def digest_md5(obj):
    obj = obj.encode('utf8')
    return md5(obj).hexdigest()


def getCSVFileDetails(url):
    match = re.search(r'\d{2}-\d{2}-\d{4}', str(url))
    date = datetime.strptime(match.group(), '%d-%m-%Y').date()
    print(date)
    typeOfTransaction = str(url).split()[0]
    print(typeOfTransaction)
    company = re.search(r'\d{4}(.*?).csv', str(url)).group(1).strip()
    company = (company.replace("_", " ").strip())
    size = url.size
    inputforHash = (company + typeOfTransaction + str(date) + str(size))
    sizeinkb = size / 1024.00
    hashuid = digest_md5(inputforHash)
    return date, company, typeOfTransaction, sizeinkb, hashuid


def getFileType(url):
    fileType = str(url).split('.')[-1]
    return fileType


def getxafheader(url):
    tree = ET.parse(url)
    ns1 = {'xaf': 'http://www.auditfiles.nl/XAF/3.1'}
    ns2 = {'xaf': 'http://www.auditfiles.nl/XAF/3.2'}
    root = tree.getroot()

    try:

        fiscalYear = root.find("./xaf:header/xaf:fiscalYear", ns1).text
        startDate = root.find("./xaf:header/xaf:startDate", ns1).text
        endDate = root.find("./xaf:header/xaf:endDate", ns1).text
        dateCreated = root.find("./xaf:header/xaf:dateCreated", ns1).text
        softwareDesc = root.find("./xaf:header/xaf:softwareDesc", ns1).text
        companyName = root.find("./xaf:company/xaf:companyName", ns1).text
        companyId = root.find("./xaf:company/xaf:companyIdent", ns1).text
        source = root.find("./xaf:header/xaf:softwareDesc", ns1).text
    except:
        fiscalYear = root.find("./xaf:header/xaf:fiscalYear", ns2).text
        startDate = root.find("./xaf:header/xaf:startDate", ns2).text
        endDate = root.find("./xaf:header/xaf:endDate", ns2).text
        dateCreated = root.find("./xaf:header/xaf:dateCreated", ns2).text
        softwareDesc = root.find("./xaf:header/xaf:softwareDesc", ns2).text
        companyName = root.find("./xaf:company/xaf:companyName", ns2).text
        companyId = root.find("./xaf:company/xaf:companyIdent", ns2).text
        source = root.find("./xaf:header/xaf:softwareDesc", ns2).text
    size = url.size
    inputforHash = (fiscalYear + startDate + endDate + dateCreated + softwareDesc + companyName + source + str(size))
    sizeinkb = size / 1024.00
    hashuid = digest_md5(inputforHash)
    xafheader = {"fiscalYear": fiscalYear,
                 "companyName": companyName,
                 "companyId": companyId,
                 "startDate": startDate,
                 "endDate": endDate,
                 "dateCreated": dateCreated,
                 "source": softwareDesc,
                 "status": "Not Processed",
                 "source": source,
                 "uid": hashuid,
                 "size": size,
                 'filesize': sizeinkb
                 }
    return xafheader
