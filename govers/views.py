import os
import shutil
from subprocess import Popen, PIPE
from time import localtime, strftime
from .forms import CasewareForm

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from django.db import IntegrityError
from django.db.models import Q
from django.shortcuts import render, redirect

from govers.models import *
from govers.xafreader import getFileType, getCSVFileDetails
from govers.xafreader import getxafheader
from .forms import XAFForm
import logging

logger = logging.getLogger('django')


def dataEntryGroup(user):
    if user:
        return user.groups.filter(name='DataEntry').count() > 0
    return False


def dashboardviewgroup(user):
    if user:
        return user.groups.filter(name='DashboardView').count() > 0
    return False


def dataEntryYukiGroup(user):
    if user:
        return user.groups.filter(name='DataEntryYuki').count() > 0
    return False


# Create your views here.
uploaded_file_url = ""


def home(request):
    xafs = EventLog.objects.exclude(status="Not Processed").order_by("processedon").reverse()
    logger.info("User is redirected to home page")
    return render(request, 'govers/home.html')


@login_required(login_url='/login/')
@user_passes_test(dashboardviewgroup, login_url='/permissiondenied/')
def dashboard(request):
    logger.info("User is redirected to Dashboard page")
    return render(request, 'govers/dashboard.html')


def logoutUser(request):
    logout(request)
    messages.success(request, "Successfully logged out")
    return redirect('/login/')


def customLogin(request):
    username = password = ''
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        valuenext = request.POST.get('next')
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                if valuenext != "":
                    return redirect(valuenext)
                else:
                    return redirect('/')
        else:
            messages.warning(request, "Invalid username or password")
    return render(request, 'govers/login.html', {'username': username})


@login_required(login_url='/login/')
@user_passes_test(dataEntryYukiGroup, login_url='/permissiondenied/')
def yuki(request):
    return render(request, 'govers/yukiapi.html')


def xafList(request):
    xafs = EventLog.objects.all().order_by("processedon")
    exactOnlineCount = xafs.filter(source='Exact Online', batchStatus="Added").count()
    YukiCount = xafs.filter(source='Yuki', batchStatus="Added").count()
    TwinfieldCount = xafs.filter(source='Twinfield', batchStatus="Added").count()
    return render(request, 'govers/upload.html',
                  {'xafs': xafs.reverse(), 'exactOnlineCount': exactOnlineCount, 'YukiCount': YukiCount,
                   'TwinfieldCount': TwinfieldCount})


@login_required(login_url='/login/')
@user_passes_test(dataEntryGroup, login_url='/permissiondenied/')
def uploadxaf(request):
    global form
    xafs = EventLog.objects.all().order_by("processedon")
    totalFileCount = xafs.filter(batchStatus="Added", status="Not Processed").count()
    currentBatchStatus = ""

    runningjobs = KtrLog.objects.filter(~Q(jobname="getMissingIDs"))
    runningjobs = list(reversed(runningjobs))

    try:
        if request.method == 'POST':
            form = XAFForm(request.POST, request.FILES)
            if form.is_valid():
                url = request.FILES.get('xaffile')

                fileType = getFileType(url)
                if fileType.endswith("xaf"):
                    res = dict()
                    res = getxafheader(url)
                    obj = EventLog()
                    obj.companyname = res.get("companyName")
                    obj.startmonth = res.get("startDate")
                    obj.endmonth = res.get("endDate")
                    obj.xaffile = request.FILES.get('xaffile')
                    obj.status = res.get("status")
                    obj.source = res.get("source")
                    obj.fiscalyear = res.get("fiscalYear")
                    obj.uid = res.get("uid")
                    obj.filesize = res.get("filesize")
                    obj.uploadedby = request.user
                    obj.batchStatus = "Not Added"
                    obj.batch_id = 0
                    obj.save()
                else:
                    date, company, typeOfTransaction, sizeinkb, hashuid = getCSVFileDetails(url)
                    obj = EventLog()
                    obj.companyname = company
                    obj.startmonth = date
                    obj.endmonth = date
                    obj.xaffile = request.FILES.get('xaffile')
                    obj.status = "Not Processed"
                    obj.source = typeOfTransaction
                    obj.fiscalyear = "N/A"
                    obj.uid = hashuid
                    obj.filesize = sizeinkb
                    obj.uploadedby = request.user
                    obj.batchStatus = "Not Added"
                    obj.batch_id = 0
                    obj.save()

                return render(request, 'govers/upload.html',
                              {'xafs': xafs.reverse(), 'totalFileCount': totalFileCount, 'runningjobs': runningjobs})
        else:
            form = XAFForm()
        return render(request, 'govers/upload.html',
                      {'xafs': xafs.reverse(), 'totalFileCount': totalFileCount, 'runningjobs': runningjobs})
    except IntegrityError:
        messages.warning(request, f'It looks like exactly same file has been processed')
        return render(request, 'govers/upload.html',
                      {'form': form, 'xafs': xafs.reverse(), 'totalFileCount': totalFileCount,
                       'runningjobs': runningjobs})


@login_required(login_url='/login/')
@user_passes_test(dataEntryGroup, login_url='/permissiondenied/')
def deletexaf(requests, pk):
    # if request.method == 'POST':'
    print("executing the delte")
    xaf = EventLog.objects.get(pk=pk)
    removeXAFForProcessing(settings.MEDIA_ROOT + "/" + str(xaf.xaffile))
    xaf.delete()
    return redirect('xaf-upload')

@login_required(login_url='/login/')
@user_passes_test(dataEntryGroup, login_url='/permissiondenied/')
def deletecaseware(request, pk):
    caseware = CaseWareFile.objects.get(pk=pk)
    removeCasewareForProcessing(settings.MEDIA_ROOT + "/" + str(caseware.casewarefile))
    caseware.delete()
    return redirect('caseware-page')


@login_required(login_url='/login/')
@user_passes_test(dataEntryGroup, login_url='/permissiondenied/')
def processxaf(request):
    xafs = EventLog.objects.all().order_by("processedon").reverse()
    countx = EventLog.objects.all()
    creditCount = countx.filter(source='Crediteuren', batchStatus="Added", status="Not Processed").count()
    debitCount = countx.filter(source='Debiteuren', batchStatus="Added", status="Not Processed").count()

    try:
        batPath = (settings.STATIC_ROOT + "\\govers\\" + settings.EXECUTABLE_FILE_NAME)
        if creditCount > 0:
            jobName = "dwh-creditorListProcessing.kjb"
        elif debitCount > 0:
            jobName = "dwh-debtorListProcessing.kjb"
        else:
            jobName = "dwh-preProcessCompany.kjb"

        if KtrLog.objects.all().order_by("-id_job").count() > 0:
            obj = KtrLog.objects.all().order_by("-id_job")[0]
            nextBatchID = obj.id_job + 1
        else:
            nextBatchID = 1
        if (EventLog.objects.filter(batchStatus="Added", status="Not Processed").count() > 0):
            allFiles = EventLog.objects.filter(batchStatus="Added", status="Not Processed").values_list("xaffile")

            file1 = ""
            file2 = ""
            file3 = ""
            file4 = ""
            file5 = ""

            if allFiles.count() >=1:
                file1 = allFiles[0][0].split("/")[-1]

            if allFiles.count() >=2:
                file2 = allFiles[1][0].split("/")[-1]

            if allFiles.count() >=3:
                file3 = allFiles[2][0].split("/")[-1]
            if allFiles.count() >=4:
                file4 = allFiles[3][0].split("/")[-1]
            
            if allFiles.count() ==5:
                file5 = allFiles[4][0].split("/")[-1]

            #logger.info("List of files being processed: [Empty is good] ===>>>>>> " + file1, file2, file3, file4, file5)
            EventLog.objects.filter(batchStatus="Added", status="Not Processed").update(batch_id=nextBatchID,
                                                                                        status="In Progress")
            msg = "Bat path: ", batPath, "Job Name; ", jobName,
            logger.info(msg)

            p = Popen([batPath, jobName, file1, file2, file3, file4, file5],  stdin=PIPE, stdout=PIPE, stderr=PIPE)    
            logger.info("Spoon batch is running now...")

            for line in iter(p.stdout.readline,''): 
                logger.info(line.rstrip()) 

            logger.info("********* Spoon processing done now ************")
            p.stdout.close()

        else:
            messages.warning(request, f'There is no file added to be processed')
    except IntegrityError:
        messages.warning(request, f'Error Occured')
        EventLog.objects.filter(batchStatus="Added", status="Not Processed").update(batch_id="", status="Error")
    return redirect('xaf-upload')



@login_required(login_url='/login/')
@user_passes_test(dataEntryGroup, login_url='/permissiondenied/')
def getMissingRGSIds(request):
    try:
        batPath = (settings.STATIC_ROOT + "/govers/" + settings.EXECUTABLE_RGS_KTR)
        msg = "Bat path: ", batPath
        logger.info("******************************")
        logger.info(msg)
        logger.info("******************************")
        p=Popen(batPath,  stdin=PIPE, stdout=PIPE, stderr=PIPE)
        logger.info(">>>>> Spoon file being executed. for more details see the log below: ")
        output, err = p.communicate(b"All the logs are being passed to the Subprocess from Bat run")
        logger.info("******************************")
        logger.info(output)
        logger.info("******************************")
        logger.info(err)
        rc = p.returncode
    finally:
        return redirect('missing-rgscodes')



@login_required(login_url='/login/')
@user_passes_test(dataEntryGroup, login_url='/permissiondenied/')
def casewarePage(request):
    casewarefiles = CaseWareFile.objects.all().order_by("-uploadedon")
    return render(request, 'govers/caseware.html', {'casewarefiles': casewarefiles})


@login_required(login_url='/login/')
@user_passes_test(dataEntryGroup, login_url='/permissiondenied/')
def uploadcaseware(request):
    casewarefiles = CaseWareFile.objects.all().order_by("uploadedon")
    if request.method == 'POST':
        form = CasewareForm(request.POST, request.FILES)
        if form.is_valid():
            url = request.FILES.get('casewarefile')
            obj = CaseWareFile()
            obj.casewarefile = request.FILES.get('casewarefile')
            obj.uploadedby = request.user
            obj.filename = request.FILES.get('casewarefile').name
            obj.status = "Not Processed"
            obj.save()
        return render(request, 'govers/caseware.html', {'casewarefiles': casewarefiles})



@login_required(login_url='/login/')
@user_passes_test(dataEntryGroup, login_url='/permissiondenied/')
def uploadCaswareSchema(request):
    try:
        casewarefiles = CaseWareFile.objects.all().order_by("uploadedon")
        batPath = (settings.STATIC_ROOT + "/govers/" + settings.EXECUTABLE_CASEWARE_KTR)
        p=Popen(batPath,  stdin=PIPE, stdout=PIPE, stderr=PIPE)
        logger.info(">>>>> Spoon file being executed. for more details see the log below: ")
        output, err = p.communicate(b"All the logs are being passed to the Subprocess from Bat run")
        logger.info("******************************")
        logger.info(output)
        logger.info("******************************")
        logger.info(err)
        rc = p.returncode
    except:
        messages.warning(f'An Error Occured')
    finally:
        return render(request, 'govers/caseware.html', {'casewarefiles': casewarefiles})


@login_required(login_url='/login/')
@user_passes_test(dataEntryGroup, login_url='/permissiondenied/')
def CaswareSchemaCheck(request):
    try:
        batPath = (settings.STATIC_ROOT + "/govers/" + settings.EXECUTABLE_CASEWARE_CHECK_KTR)

        p=Popen(batPath,  stdin=PIPE, stdout=PIPE, stderr=PIPE)
        logger.info(">>>>> Spoon file being executed. for more details see the log below: ")
        output, err = p.communicate(b"All the logs are being passed to the Subprocess from Bat run")
        logger.info("******************************")
        logger.info(output)
        logger.info("******************************")
        logger.info(err)
        rc = p.returncode
    except:
        print(batPath)
        messages.warning(f'An Error Occured')
    finally:
        print(batPath)
        return redirect('missing-rgscodes')



@login_required(login_url='/login/')
@user_passes_test(dataEntryGroup, login_url='/permissiondenied/')
def missingrgsidPage(request):
    fileList =[]
    fileList = getmissingreports(settings.MEDIA_ROOT + "/rgs/")
    return render(request, 'govers/rgscodes.html', {'files': fileList})


def addToBatch(request, pk):
    xaf = EventLog.objects.get(pk=pk)
    if EventLog.objects.filter(batchStatus="Added", status="Not Processed").count() > 4:
        messages.warning(request, f'Maximum 5 Files can be processed in one batch')
        return redirect('xaf-upload')
    elif ("credit" in xaf.source.lower()) and EventLog.objects.filter(~Q(source="Crediteuren"), batchStatus="Added",
                                                                      status="Not Processed").count() > 0:
        messages.warning(request, f'There is already a Debiteuren file added for processing... remove it before '
                                  f'processing Crediteuren')
        return redirect('xaf-upload')

    elif ("debit" in xaf.source.lower()) and EventLog.objects.filter(~Q(source="Debiteuren"), batchStatus="Added",
                                                                     status="Not Processed").count() > 0:
        messages.warning(request, f'There is already a Crediteuren file added for processing... remove it before '
                                  f'processing Debiteuren')
        return redirect('xaf-upload')

    elif (("debit" not in xaf.source.lower() and "credit" not in xaf.source.lower())
          and (EventLog.objects.filter(batchStatus="Added", status="Not Processed", source="Crediteuren").count() > 0
               or EventLog.objects.filter(batchStatus="Added", status="Not Processed",
                                          source="Debiteuren").count() > 0)):

        messages.warning(request, f'There is already a Crediteuren/Debiteuren file added for processing... remove it '
                                  f'before processing any other type of file')
        return redirect('xaf-upload')
    else:
        try:
            if copyXAFForProcessing(settings.MEDIA_ROOT + "/" + str(xaf.xaffile)):
                xaf.batchStatus = "Added"
                xaf.save()
        except IntegrityError:
            messages.error(request, f'Error Occured')
        return redirect('xaf-upload')





def removeFromBatch(request, pk):
    xaf = EventLog.objects.get(pk=pk)
    head, tail = os.path.split(str(xaf.xaffile))
    if removeXAFForProcessing(settings.TEMP_XAF_FOLDER + "/" + tail):
        xaf.batchStatus = "Not Added"
        xaf.save()
    return redirect('xaf-upload')

def addCasewareTobatch(request, pk):
    caseware = CaseWareFile.objects.get(pk=pk)
    try:
        if copyCasewareForProcessing(settings.MEDIA_ROOT + "/" + str(caseware.casewarefile)):
            caseware.status = "Added"
            caseware.save()
    except IntegrityError:
        messages.error(request, f'Error Occured')
    return redirect('caseware-page')


def removeCasewareFromBatch(request,pk):
    caseware = CaseWareFile.objects.get(pk=pk)
    head, tail = os.path.split(str(caseware.casewarefile))
    if removeCasewareForProcessing(settings.TEMP_CASEWARE_FOLDER + "/" + tail):
        caseware.status = "Not Processed"
        caseware.save()
    return redirect('caseware-page')


@login_required(login_url='/login/')
@user_passes_test(dataEntryGroup, login_url='/permissiondenied/')
def processingdetails(request, pk):
    try:
        obj = KtrLog.objects.get(pk=pk)
        res = obj.log_field.splitlines()
        return render(request, 'govers/processing.html', {'logs': list(reversed(res)), 'status': obj.status})
    except:
        return render(request, 'govers/processing.html',
                      {'logs': (["Wait a while for log to appear here.... "]), 'status': "Wait...."})


def copyXAFForProcessing(url):
    try:
        print(url)
        print(settings.TEMP_XAF_FOLDER)
        shutil.copy(url, settings.TEMP_XAF_FOLDER)
        return True
    except:
        return False


def removeXAFForProcessing(url):
    try:
        print(url)
        os.unlink(url)
        return True
    except IntegrityError:
        logger.error(f'Error Occurred')


def copyCasewareForProcessing(url):
    try:
        shutil.copy(url, settings.TEMP_CASEWARE_FOLDER)
        return True
    except:
        return False


def removeCasewareForProcessing(url):
    try:
        os.unlink(url)
        return True
    except IntegrityError:
        logger.error(f'Error Occurred')


@login_required
def viewprofile(request):
    return render(request, 'govers/profile.html')


def permission_denied(request):
    return render(request, 'govers/permissiondenied.html')


def getmissingreports(directory):
    filelist = []
    for filename in os.listdir(directory):
        path = os.path.join(directory, filename)
        (mode, ino, dev, nlink, uid, gid, size,
         atime, mtime, ctime) = os.stat(path)
        filelist.append({'file': filename, 'path': path, 'modifiedon': strftime(
            "%d-%b-%Y %H:%M:%S", localtime(mtime))})
        logger.info(path)
    logger.info("List of missing files" + ";".join(filelist))
    return filelist

