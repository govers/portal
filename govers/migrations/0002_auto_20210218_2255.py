# Generated by Django 2.1 on 2021-02-18 21:55

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('govers', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CaseWareFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('filename', models.CharField(max_length=250)),
                ('casewarefile', models.FileField(upload_to='C:/Users/vishw/projects/business/govers/repos/etl/etl/temp')),
                ('uploadedby', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='KtrLog',
            fields=[
                ('id_job', models.IntegerField(primary_key=True, serialize=False)),
                ('channel_id', models.CharField(max_length=250)),
                ('jobname', models.CharField(max_length=250)),
                ('status', models.CharField(max_length=250)),
                ('lines_read', models.BigIntegerField(default=0)),
                ('lines_written', models.BigIntegerField(default=0)),
                ('lines_updated', models.BigIntegerField(default=0)),
                ('lines_input', models.BigIntegerField(default=0)),
                ('lines_output', models.BigIntegerField(default=0)),
                ('lines_rejected', models.BigIntegerField(default=0)),
                ('errors', models.CharField(max_length=250)),
                ('startdate', models.DateTimeField(default='01/01/1900')),
                ('enddate', models.DateTimeField(default='01/01/1900')),
                ('logdate', models.DateTimeField(default='01/01/1900')),
                ('depdate', models.DateTimeField(default='01/01/1900')),
                ('replaydate', models.DateTimeField(default='01/01/1900')),
                ('log_field', models.TextField()),
            ],
        ),
        migrations.AddField(
            model_name='eventlog',
            name='batchStatus',
            field=models.CharField(default=' ', max_length=150),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='eventlog',
            name='batch_id',
            field=models.BigIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='processinglog',
            name='uid',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='govers.EventLog'),
        ),
    ]
